import { defineConfig } from 'vite';
import { resolve } from 'path';
import handlebars from 'vite-plugin-handlebars';

export default defineConfig({
    css: {
        devSourcemap: true,
    },
    plugins: [
        handlebars({
            partialDirectory: resolve(__dirname, 'src', 'partials'),
        }),
    ],
    // multi page build setup
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                contact: resolve(__dirname, 'contact.html'),
            },
        },
    },
})
